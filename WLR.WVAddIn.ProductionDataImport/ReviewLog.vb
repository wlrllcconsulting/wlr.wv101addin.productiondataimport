﻿Imports System.IO
Public Class ReviewLog
    Private sLogHeader As String = ""
    Private sLogMessage As String = ""

    Friend Sub LogFile_Create(ByVal sFile As String, ByVal iCompleted As Long, ByVal iWarnings As Long, ByVal iErrors As Long, ByVal sErrorMessage As String)
        Dim sbLogHeader As New System.Text.StringBuilder
        Dim sbLogMessage As New System.Text.StringBuilder
        Dim sIndent As String = "".PadLeft(5)

        sbLogHeader.AppendLine("========================================")
        sbLogHeader.AppendLine(modGlobal.Title + " - Import Log")
        sbLogHeader.AppendLine("Created On: " & Date.Now.ToString)
        sbLogHeader.AppendLine("========================================")
        sbLogHeader.AppendLine("The import operation has completed with:")
        sbLogHeader.AppendLine()
        sbLogHeader.AppendLine(String.Format("{0}{1} Warning(s)", sIndent, iWarnings))
        sbLogHeader.AppendLine(String.Format("{0}{1} Error(s)", sIndent, iErrors))
        sbLogHeader.AppendLine(String.Format("{0}A total of {1} record(s) were processed.", sIndent, iCompleted))

        sbLogMessage.AppendLine()
        sbLogMessage.AppendLine("Log Output:")
        sbLogMessage.AppendLine(sErrorMessage)

        sLogHeader = sbLogHeader.ToString
        sLogMessage = sbLogMessage.ToString
    End Sub

    Private Sub diag_importComplete_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ControlBox = False
        txtLog.Text = sLogHeader
    End Sub

    Private Sub btn_viewLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_viewLog.Click

        If Me.pnl_log.Visible = True Then
            Me.pnl_log.Visible = False
            Me.Width = 345
            Me.Height = 205
            Me.btn_viewLog.Text = "View Log"
        Else
            'WebBrowser1.DocumentText = WellHeaderDialog.strImportLog
            txtLogView.Text = sLogHeader + sLogMessage

            Me.Height = 620
            Me.Width = 696
            Me.pnl_log.Show()
            Me.btn_viewLog.Text = "Close Log"
        End If

    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub btn_saveLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_saveLog.Click
        Me.diag_saveLog.ShowDialog()
    End Sub

    Private Sub SaveFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles diag_saveLog.FileOk

        Dim strFileName As String = System.IO.Path.GetFullPath(diag_saveLog.FileName)
        Dim oFile As System.IO.Stream

        Try
            oFile = System.IO.File.Create(strFileName)
            oFile.Close()
            My.Computer.FileSystem.WriteAllText(strFileName, sLogHeader + sLogMessage, False)

            If System.IO.File.Exists(strFileName) Then
                System.Windows.Forms.MessageBox.Show("Log file saved.", "File Saved", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information)
            End If

            oFile.Close()
            oFile.Dispose()
        Catch ex As Exception

            System.Windows.Forms.MessageBox.Show(ex.Message, "File Save Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)

        End Try

    End Sub

End Class
Imports Peloton.AppFrame.IO
Imports Peloton.DataModel.WellView101
Imports Excel = Microsoft.Office.Interop.Excel

Public Class clsImport
    Private mclsGeneral As New clsGeneral
    Private mIO As IOEngine = modGlobal.IOEngine

    Private mdicExcelMappings As Dictionary(Of String, Integer)
    Private Const msDelimiter As String = "~"

    Private mProductionTable As Table = Nothing
    Private mProductionTableData As TableData = Nothing
    Private mProductionDownTimeTable As Table = Nothing
    Private mProductionDownTimeTableData As TableData = Nothing
    Private mProductionGasTable As Table = Nothing
    Private mProductionGasTableData As TableData = Nothing
    Private mProductionLiquidTable As Table = Nothing
    Private mProductionLiquidTableData As TableData = Nothing
    Private mProdSettingTable As Table = Nothing
    Private mProdSettingTableData As TableData = Nothing
    Private mProdSettingFlowTable As Table = Nothing
    Private mProdSettingFlowTableData As TableData = Nothing

    Public Sub New(ByRef dicExcelMappings As Dictionary(Of String, Integer))
        mdicExcelMappings = dicExcelMappings
        mProductionTable = mIO.Tables(dm.Production.Name)
        mProductionDownTimeTable = mIO.Tables(dm.ProductionDownTime.Name)
        mProductionGasTable = mIO.Tables(dm.ProductionGas.Name)
        mProductionLiquidTable = mIO.Tables(dm.ProductionLiquid.Name)
        mProdSettingTable = mIO.Tables(dm.ProdSetting.Name)
        mProdSettingFlowTable = mIO.Tables(dm.ProdSettingFlow.Name)
    End Sub

    Public Sub Dispose()
        mProductionTable = Nothing
        mProductionTableData = Nothing
        mProductionDownTimeTable = Nothing
        mProductionDownTimeTableData = Nothing
        mProductionGasTable = Nothing
        mProductionGasTableData = Nothing
        mProductionLiquidTable = Nothing
        mProductionLiquidTableData = Nothing
        mProdSettingTable = Nothing
        mProdSettingTableData = Nothing
        mProdSettingFlowTable = Nothing
        mProdSettingFlowTableData = Nothing
    End Sub

    Public Function ImportData(ByRef xlSheet As Excel.Worksheet, ByVal sRow As String, ByVal sIDWell As String, ByVal sWellName As String, ByVal sDate As String, ByRef iCompleted As Long, ByRef iWarnings As Long, ByRef iErrors As Long) As String
        Dim sErrMessage As String = ""
        Dim sSection As String = "Initializing"
        Try
            Dim iRow As Integer = 0
            If Not Integer.TryParse(sRow, iRow) Then
                sErrMessage += "Invalid Row Number: (Row #" + sRow + ") " + sWellName + " - " + sDate + Environment.NewLine
                iErrors += 1
                Return sErrMessage
                Exit Function
            End If

            Dim dProductionDate As DateTime = Date.MinValue
            If Not DateTime.TryParse(sDate, dProductionDate) Then
                sErrMessage += "Invalid Date: (Row #" + sRow + ") " + sWellName + " - " + sDate + Environment.NewLine
                iErrors += 1
                Return sErrMessage
                Exit Function
            End If

            sSection = "wvProduction"
            Dim sFilter As String = "DtTmStart=#" + dProductionDate + "#"
            Dim sIDRecProduction As String = ImportTable(xlSheet, iRow, sSection, sIDWell, sIDWell, sFilter)

            sSection = "wvProductionDownTime"
            sFilter = "DtTmStart=#" + dProductionDate + "# AND IDRecParent='" + sIDRecProduction + "'"
            ImportTable(xlSheet, iRow, sSection, sIDWell, sIDRecProduction, sFilter)

            sSection = "wvProductionGas"
            sFilter = "ProductTyp='Reservoir Gas' AND IDRecParent='" + sIDRecProduction + "'"
            ImportTable(xlSheet, iRow, sSection, sIDWell, sIDRecProduction, sFilter)

            sSection = "wvProductionLiquid_Oil"
            sFilter = "ProductTyp='Oil' AND IDRecParent='" + sIDRecProduction + "'"
            ImportTable(xlSheet, iRow, sSection, sIDWell, sIDRecProduction, sFilter)

            sSection = "wvProductionLiquid_Water"
            sFilter = "ProductTyp='Water' AND IDRecParent='" + sIDRecProduction + "'"
            ImportTable(xlSheet, iRow, sSection, sIDWell, sIDRecProduction, sFilter)

            sSection = "wvProdSetting"
            sFilter = "DtTmStart=#" + dProductionDate + "#"
            Dim sIDRecProdSetting As String = ImportTable(xlSheet, iRow, sSection, sIDWell, sIDWell, sFilter)

            sSection = "wvProdSettingFlow"
            sFilter = "IDRecParent='" + sIDRecProduction + "'"
            ImportTable(xlSheet, iRow, sSection, sIDWell, sIDRecProdSetting, sFilter)

            sErrMessage += "Successfully Imported:  (Row #" + sRow + ") " + sWellName + " - " + sDate + Environment.NewLine
            iCompleted += 1

            Return sErrMessage
        Catch ex As Exception
            sErrMessage += "Application Error Occurred: Table - " + sSection + ex.Message + Environment.NewLine
            iErrors += 1
            Return sErrMessage
        Finally

        End Try

    End Function

    Private Function ImportTable(ByRef xlSheet As Excel.Worksheet, ByVal iRow As Integer, ByVal sTableName As String, ByVal sIDWell As String, ByVal sIDRecParent As String, ByVal sFilter As String) As String
        Dim bUpdate As Boolean = False

        Dim sWVTableName As String = sTableName.ToLower
        If sTableName.IndexOf("_") >= 0 Then
            sWVTableName = sTableName.Substring(0, sTableName.IndexOf("_")).ToLower
        End If
        'Dim WVTable As Table = mIO.Tables(sWVTableName)
        'Dim WVTableData As TableData = WVTable.GetData(sIDWell)

        Dim WVTableData As TableData = Nothing
        Select Case sWVTableName
            Case dm.Production.Name
                If mProductionTableData Is Nothing OrElse mProductionTableData.EntityId <> sIDWell Then
                    mProductionTableData = mProductionTable.GetData(sIDWell)
                End If
                WVTableData = mProductionTableData
            Case dm.ProductionDownTime.Name
                If mProductionDownTimeTableData Is Nothing OrElse mProductionDownTimeTableData.EntityId <> sIDWell Then
                    mProductionDownTimeTableData = mProductionDownTimeTable.GetData(sIDWell)
                End If
                WVTableData = mProductionDownTimeTableData
            Case dm.ProductionGas.Name
                If mProductionGasTableData Is Nothing OrElse mProductionGasTableData.EntityId <> sIDWell Then
                    mProductionGasTableData = mProductionGasTable.GetData(sIDWell)
                End If
                WVTableData = mProductionGasTableData
            Case dm.ProductionLiquid.Name
                If mProductionLiquidTableData Is Nothing OrElse mProductionLiquidTableData.EntityId <> sIDWell Then
                    mProductionLiquidTableData = mProductionLiquidTable.GetData(sIDWell)
                End If
                WVTableData = mProductionLiquidTableData
            Case dm.ProdSetting.Name
                If mProdSettingTableData Is Nothing OrElse mProdSettingTableData.EntityId <> sIDWell Then
                    mProdSettingTableData = mProdSettingTable.GetData(sIDWell)
                End If
                WVTableData = mProdSettingTableData
            Case dm.ProdSettingFlow.Name
                If mProdSettingFlowTableData Is Nothing OrElse mProdSettingFlowTableData.EntityId <> sIDWell Then
                    mProdSettingFlowTableData = mProdSettingFlowTable.GetData(sIDWell)
                End If
                WVTableData = mProdSettingFlowTableData
        End Select

        Dim WVRecord As TableDataRecord
        Dim lstRemove As New List(Of String)

        WVTableData.Records.Filter(sFilter)
        If WVTableData.Records.Count > 0 Then
            WVRecord = WVTableData.Records.Item(0)
            For iRecord As Integer = 1 To WVTableData.Records.Count - 1
                lstRemove.Add(WVTableData.Records.Item(iRecord).UniqueId)
            Next
        Else
            'add new record
            WVRecord = WVTableData.Records.Add(sIDRecParent)
        End If
        Dim lstFields As Dictionary(Of String, String) = mclsGeneral.IniResultCollection(sTableName)
        For Each okeyFields As KeyValuePair(Of String, String) In lstFields
            If okeyFields.Value.IndexOf("'") >= 0 Then
                WVRecord.ItemRaw(okeyFields.Key.ToLower) = okeyFields.Value.Substring(1, okeyFields.Value.Length - 2)
            Else
                WVRecord.ItemConverted(okeyFields.Key.ToLower) = xlSheet.Cells(iRow, mdicExcelMappings.Item(okeyFields.Value)).Value
            End If
        Next
        WVRecord.ItemRaw(dm.SystemFields.sysTag.Name) = modGlobal.Tag
        For Each sID As String In lstRemove
            WVTableData.Records.Remove(sID)
        Next
        WVTableData.Update(modGlobal.Tag, False)

        Return WVRecord.UniqueId
        WVTableData = Nothing
        'WVTable = Nothing
    End Function
End Class

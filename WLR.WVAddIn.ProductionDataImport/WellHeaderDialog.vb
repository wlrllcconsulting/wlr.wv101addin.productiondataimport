﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Peloton.AppFrame.IO

Public Class WellHeaderDialog
    Private mclsGeneral As New clsGeneral
    Private mIO As IOEngine = modGlobal.IOEngine

    Private mxlApp As Excel.Application = Nothing
    Private mxlWorkBook As Excel.Workbook = Nothing
    Private mxlSheet As Excel.Worksheet = Nothing

    Private mdicExcelHeaderMappings As New Dictionary(Of String, Integer)
    Private msFile As String = ""
    Private Const mcsDelimiter As String = "~"

    Public Sub New(ByVal sFile As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        msFile = sFile
        mclsGeneral.DBConnection = modGlobal.IODBConnection
    End Sub


    Private Sub WellHeaderDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim frmProgress As New ProgressForm
            frmProgress.Text = "Retrieving Excel Sheet..."
            frmProgress.lblFile.Text = "File: " + System.IO.Path.GetFileName(msFile)
            frmProgress.Show()

            Dim sWellIdentifer As String() = Nothing
            Dim sSheetName As String = "Production Data"
            Dim iRowHeader As Integer = 1
            Dim iRowDataStart As Integer = 2
            Dim lstSettings As Dictionary(Of String, String) = mclsGeneral.IniResultCollection("Settings")
            If lstSettings.ContainsKey("WellIdentifier") Then
                'find the well identifier
                If lstSettings.Item("WellIdentifier").IndexOf("|") > 0 Then
                    sWellIdentifer = lstSettings.Item("WellIdentifier").Split("|")
                Else
                    Throw New Exception("Well Identifer not set correctly in the ini file.")
                End If
            End If
            If lstSettings.ContainsKey("ExcelSheetName") Then sSheetName = lstSettings("ExcelSheetName")
            If lstSettings.ContainsKey("ExcelHeaderRow") Then Integer.TryParse(lstSettings("ExcelHeaderRow"), iRowHeader)
            If lstSettings.ContainsKey("ExcelDataRowStart") Then Integer.TryParse(lstSettings("ExcelDataRowStart"), iRowDataStart)
            If lstSettings.ContainsKey("ImportTagName") AndAlso lstSettings.Item("ImportTagName").Length > 0 Then modGlobal.Tag = lstSettings.Item("ImportTagName")

            Dim sDate As String = "Date"
            Dim lstProd As Dictionary(Of String, String) = mclsGeneral.IniResultCollection("wvProduction")
            If lstProd.ContainsKey("DtTmStart") Then sDate = lstProd("DtTmStart")

            mxlApp = New Excel.Application
            mxlWorkBook = mxlApp.Workbooks.Open(msFile)

            Dim iColWellIdentifer As Integer = 0
            Dim iColDate As Integer = 0
            For Each xlSheet In mxlWorkBook.Sheets
                If xlSheet.Name = sSheetName Then
                    mxlSheet = xlSheet
                    For iCol As Integer = 1 To xlSheet.UsedRange.Columns.Count
                        If xlSheet.Cells(iRowHeader, iCol).Text.ToString.Length > 0 Then
                            If Not mdicExcelHeaderMappings.ContainsKey(xlSheet.Cells(iRowHeader, iCol).Value.ToString) Then
                                mdicExcelHeaderMappings.Add(xlSheet.Cells(iRowHeader, iCol).Value.ToString, iCol)
                            End If
                            If xlSheet.Cells(iRowHeader, iCol).Value.ToString.ToLower = sWellIdentifer(0).ToLower Then
                                iColWellIdentifer = iCol
                            ElseIf xlSheet.Cells(iRowHeader, iCol).Value.ToString.ToLower = sDate.ToLower Then
                                iColDate = iCol
                            End If
                        End If
                    Next

                    If iColWellIdentifer > 0 AndAlso iColDate > 0 Then
                        Dim dicWells As New Dictionary(Of String, String)
                        For iRow As Integer = iRowDataStart To xlSheet.UsedRange.Rows.Count
                            frmProgress.barProgress.Value = 100 * iRow / xlSheet.UsedRange.Rows.Count
                            frmProgress.lblProgress.Text = "Reading Row #" + iRow.ToString + " out of " + xlSheet.UsedRange.Rows.Count.ToString
                            frmProgress.Refresh()

                            Dim sIDWell As String = ""
                            Dim sWellName As String = ""
                            Dim dProduction As DateTime = Date.MinValue
                            If xlSheet.Cells(iRow, iColWellIdentifer).Text.ToString.Trim.Length > 0 Then
                                sWellName = xlSheet.Cells(iRow, iColWellIdentifer).Value.ToString.Trim
                                If dicWells.ContainsKey(sWellName) Then
                                    sIDWell = dicWells.Item(sWellName)
                                Else
                                    Dim dsData As New DataSet
                                    mclsGeneral.DBFillDS(dsData, "Well", "SELECT IDWell FROM wvWellHeader WHERE WellName='" + sWellName + "'")
                                    If Not dsData Is Nothing AndAlso dsData.Tables("Well").Rows.Count > 0 Then
                                        sIDWell = dsData.Tables("Well").Rows(0).Item("IDWell").ToString
                                        dicWells.Add(sWellName, sIDWell)
                                    End If
                                    dsData = Nothing
                                End If
                            End If
                            If xlSheet.Cells(iRow, iColDate).Text.ToString.Trim.Length > 0 Then DateTime.TryParse(xlSheet.Cells(iRow, iColDate).Value.ToString.Trim, dProduction)

                            If sIDWell.Length = 0 Then
                                dgvWellSelect.Rows.Add({sWellName, dProduction, False, False, False, sIDWell, iRow})
                            ElseIf dProduction = Date.MinValue Then
                                dgvWellSelect.Rows.Add({sWellName, xlSheet.Cells(iRow, iColDate).Text.ToString, True, False, False, sIDWell, iRow})
                            Else
                                Dim dsData As New DataSet
                                mclsGeneral.DBFillDS(dsData, "Production", "SELECT IDRec, DtTmStart FROM wvProduction WHERE IDWell='" + sIDWell + "'")
                                If Not dsData Is Nothing Then
                                    Dim drRows() As DataRow = dsData.Tables("Production").Select("DtTmStart = #" + dProduction + "#")
                                    If drRows.Length > 0 Then
                                        dgvWellSelect.Rows.Add({sWellName, dProduction, True, True, True, sIDWell, iRow})
                                    Else
                                        dgvWellSelect.Rows.Add({sWellName, dProduction, True, False, True, sIDWell, iRow})
                                    End If
                                End If
                            End If
                        Next
                    Else
                        Throw New Exception("Well Identifer (" + sWellIdentifer(0) + ") and Production Start Date (" + sDate + ") columns not found.  Please check ini file settings.")
                    End If
                    Exit For
                End If
            Next
            frmProgress.Close()

            For iRow As Integer = 0 To dgvWellSelect.Rows.Count - 1
                If dgvWellSelect.Rows.Item(iRow).Cells("colImport").Value = False Then
                    dgvWellSelect.Rows.Item(iRow).Cells("colImport").ReadOnly = True
                    dgvWellSelect.Rows.Item(iRow).DefaultCellStyle.BackColor = Drawing.Color.Beige
                End If
            Next
            Me.ControlBox = False

            dgvWellSelect.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
            'dgvWellSelect.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None
            dgvWellSelect.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
            dgvWellSelect.Refresh()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Error Occurred: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK)

            Me.Close()
        Finally

        End Try

    End Sub

    Private Sub WellHeaderDialog_FormClosing(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosing
        If Not (mxlWorkBook Is Nothing) Then
            mxlWorkBook.Save()
            mxlWorkBook.Close()
        End If

        If Not (mxlApp Is Nothing) Then mxlApp.Quit()

        mxlSheet = Nothing
        mxlWorkBook = Nothing
        mxlApp = Nothing
    End Sub

    Private Sub btn_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_import.Click
        Dim frmProgress As New ProgressForm

        Try
            Dim oImport As New clsImport(mdicExcelHeaderMappings)
            Dim iCompleted As Long = 0
            Dim iErrors As Long = 0
            Dim iWarnings As Long = 0
            Dim sMessage As String = ""

            frmProgress.barProgress.Value = 0
            frmProgress.lblFile.Text = ""
            frmProgress.lblProgress.Text = "Initializing Import.  Please Wait..."
            frmProgress.Show()

            sMessage = modGlobal.Title + " Started --------------- " + Date.Now.ToString + Environment.NewLine
            Dim iProgress As Integer = 1
            For iRow As Long = 0 To dgvWellSelect.Rows.Count - 1
                Try
                    If dgvWellSelect.Rows(iRow).Cells("colImport").Value Then
                        frmProgress.barProgress.Value = 100 * iRow / dgvWellSelect.Rows.Count
                        frmProgress.lblProgress.Text = "Updating " + dgvWellSelect.Rows(iRow).Cells("WellName").Value.ToString + " - " + dgvWellSelect.Rows(iRow).Cells("DtTm").Value.ToString
                        frmProgress.Refresh()

                        sMessage += oImport.ImportData(mxlSheet, dgvWellSelect.Rows(iRow).Cells("ExcelRow").Value.ToString, dgvWellSelect.Rows(iRow).Cells("IDWell").Value.ToString, dgvWellSelect.Rows(iRow).Cells("WellName").Value.ToString, _
                                                       dgvWellSelect.Rows(iRow).Cells("DtTm").Value.ToString, iCompleted, iWarnings, iErrors)
                        iProgress += 1
                    Else
                        sMessage += "Not Selected to be Imported: (Row #" + dgvWellSelect.Rows(iRow).Cells("ExcelRow").Value.ToString + ") " + dgvWellSelect.Rows(iRow).Cells("WellName").Value.ToString + " - " + dgvWellSelect.Rows(iRow).Cells("DtTm").Value.ToString + Environment.NewLine
                    End If

                Catch ex As Exception
                    sMessage += "Error: " + ex.Message + Environment.NewLine
                End Try
            Next
            sMessage += modGlobal.Title + " Completed --------------- " + Date.Now.ToString + Environment.NewLine
            frmProgress.Close()

            oImport.Dispose()

            Dim frmReviewLog As New ReviewLog
            frmReviewLog.LogFile_Create("", iCompleted, iWarnings, iErrors, sMessage)
            Me.Close()
            frmReviewLog.ShowDialog()

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Error Occurred: " + ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK)
            If Not frmProgress.IsDisposed Then
                frmProgress.Dispose()
            End If
        Finally

            Me.Close()
        End Try
    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmdPrint_Click(sender As System.Object, e As System.EventArgs) Handles cmdSave.Click
        SaveFileDialog1.ShowDialog()
    End Sub

    Private Sub SaveFileDialog1_FileOk(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        Dim strFileName As String = System.IO.Path.GetFullPath(SaveFileDialog1.FileName)
        Dim oFile As System.IO.Stream

        Try
            Dim sMessage As String = ""

            'header
            For iCol As Integer = 0 To dgvWellSelect.ColumnCount - 1
                sMessage += dgvWellSelect.Columns(iCol).HeaderText + ", "
            Next
            sMessage += Environment.NewLine
            'data
            For iRow As Integer = 0 To dgvWellSelect.RowCount - 1
                For iCol As Integer = 0 To dgvWellSelect.ColumnCount - 1
                    If dgvWellSelect.Rows(iRow).Cells(dgvWellSelect.Columns(iCol).Name).Value Is Nothing Then
                        sMessage += " , "
                    ElseIf dgvWellSelect.Rows(iRow).Cells(dgvWellSelect.Columns(iCol).Name).Value.ToString.IndexOf(",") >= 0 Then
                        sMessage += """" + dgvWellSelect.Rows(iRow).Cells(dgvWellSelect.Columns(iCol).Name).Value.ToString + """, "
                    Else
                        sMessage += dgvWellSelect.Rows(iRow).Cells(dgvWellSelect.Columns(iCol).Name).Value.ToString + ", "
                    End If

                Next
                sMessage += Environment.NewLine
            Next

            oFile = System.IO.File.Create(strFileName)
            oFile.Close()
            My.Computer.FileSystem.WriteAllText(strFileName, sMessage, False)
            If System.IO.File.Exists(strFileName) Then
                Process.Start(strFileName)
            End If
            oFile.Close()
            oFile.Dispose()


        Catch ex As Exception

            System.Windows.Forms.MessageBox.Show(ex.Message, "File Save Error")

        End Try

    End Sub
End Class
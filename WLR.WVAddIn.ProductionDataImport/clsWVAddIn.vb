﻿Imports Peloton.AppFrame.IO

Public Class clsWVAddIn
    Implements Peloton.AppFrame.Interfaces.IAddin

    ' Initialization
    Private Const mcProductName As String = "WLR Production Data Import"                  ' Add-In Name as it appears in Add-In menu
    Private Const mcsFileNameBase As String = "WLR.WVAddIn.ProductionDataImport"
    Private Const mcRequiresEntity As Boolean = False                                                ' Add-In ensures that at least one flow network is selected prior to launching process
    Private Const mcAllowMultiEntity As Boolean = True                                              ' Multi flow networks are allowed prior to launching process

    Private mIO As IOEngine

    Public Function Run(ByVal ioEngine As Object, ByVal entityIds As System.Collections.Generic.List(Of String)) As System.Collections.Generic.List(Of String) Implements Peloton.AppFrame.Interfaces.IAddin.Run
        ' Try
        If ioEngine Is Nothing Then Throw New ArgumentNullException("ioEngine")
        If entityIds Is Nothing Then entityIds = New List(Of String)

        mIO = DirectCast(ioEngine, IOEngine)
        modGlobal.IOEngine = mIO

        Dim sIDWell As String = ""

        Dim frmImport As New ImportDialog
        If frmImport.ShowDialog = System.Windows.Forms.DialogResult.OK Then
            Return entityIds
        Else
            Return Nothing
        End If

        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message, "Error", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error)
        '    Return entityIds
        'Finally
        '    ' Run Garbage Collector twice 
        '    GC.Collect()
        '    GC.WaitForPendingFinalizers()
        '    GC.Collect()
        '    GC.WaitForPendingFinalizers()
        'End Try
    End Function

    Public ReadOnly Property AllowMultipleEntities() As Boolean Implements Peloton.AppFrame.Interfaces.IAddinBase.AllowMultipleEntities
        Get
            Return mcAllowMultiEntity
        End Get
    End Property

    Public ReadOnly Property Name() As String Implements Peloton.AppFrame.Interfaces.IAddinBase.Name
        Get
            Dim sFileName As String = System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly.Location.ToString)
            If sFileName.IndexOf(mcsFileNameBase) >= 0 Then
                sFileName = sFileName.Replace(mcsFileNameBase, "").Replace(".", "").Trim
                If sFileName = "" Then sFileName = mcProductName
            Else
                sFileName = "NOT LICENSED BY WLR-LLC"
            End If
            modGlobal.Title = sFileName
            Return sFileName
        End Get
    End Property

    Public ReadOnly Property RequiresEntity() As Boolean Implements Peloton.AppFrame.Interfaces.IAddinBase.RequiresEntity
        Get
            Return mcRequiresEntity
        End Get
    End Property

End Class

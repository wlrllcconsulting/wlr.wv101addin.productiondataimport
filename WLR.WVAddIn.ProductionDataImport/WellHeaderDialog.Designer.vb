﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WellHeaderDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvWellSelect = New System.Windows.Forms.DataGridView()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_import = New System.Windows.Forms.Button()
        Me.lbl_intro = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tp_selectAll = New System.Windows.Forms.ToolTip(Me.components)
        Me.tp_deselectAll = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.WellName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtTm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WellExists = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DateExists = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colImport = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IDWell = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExcelRow = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvWellSelect, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvWellSelect
        '
        Me.dgvWellSelect.AllowUserToAddRows = False
        Me.dgvWellSelect.AllowUserToDeleteRows = False
        Me.dgvWellSelect.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvWellSelect.ColumnHeadersHeight = 35
        Me.dgvWellSelect.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WellName, Me.DtTm, Me.WellExists, Me.DateExists, Me.colImport, Me.IDWell, Me.ExcelRow})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvWellSelect.Location = New System.Drawing.Point(6, 19)
        Me.dgvWellSelect.Name = "dgvWellSelect"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvWellSelect.RowHeadersVisible = False
        Me.dgvWellSelect.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvWellSelect.RowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvWellSelect.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvWellSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWellSelect.Size = New System.Drawing.Size(369, 370)
        Me.dgvWellSelect.TabIndex = 0
        '
        'btn_cancel
        '
        Me.btn_cancel.Location = New System.Drawing.Point(309, 433)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancel.TabIndex = 1
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'btn_import
        '
        Me.btn_import.Location = New System.Drawing.Point(228, 433)
        Me.btn_import.Name = "btn_import"
        Me.btn_import.Size = New System.Drawing.Size(75, 23)
        Me.btn_import.TabIndex = 2
        Me.btn_import.Text = "Import"
        Me.btn_import.UseVisualStyleBackColor = True
        '
        'lbl_intro
        '
        Me.lbl_intro.AutoSize = True
        Me.lbl_intro.Location = New System.Drawing.Point(0, 9)
        Me.lbl_intro.Name = "lbl_intro"
        Me.lbl_intro.Size = New System.Drawing.Size(239, 13)
        Me.lbl_intro.TabIndex = 3
        Me.lbl_intro.Text = " Please select the well(s) you would like to import."
        Me.lbl_intro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvWellSelect)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 25)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(381, 402)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(3, 433)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 5
        Me.cmdSave.Text = "Save To File"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.Filter = "Excel delimited files (*.csv)|*.csv|All files (*.*)|*.*"
        '
        'WellName
        '
        Me.WellName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.WellName.DefaultCellStyle = DataGridViewCellStyle2
        Me.WellName.HeaderText = "Well Name"
        Me.WellName.MinimumWidth = 60
        Me.WellName.Name = "WellName"
        Me.WellName.ReadOnly = True
        '
        'DtTm
        '
        Me.DtTm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DtTm.DefaultCellStyle = DataGridViewCellStyle3
        Me.DtTm.HeaderText = "Date"
        Me.DtTm.Name = "DtTm"
        Me.DtTm.ReadOnly = True
        '
        'WellExists
        '
        Me.WellExists.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.NullValue = False
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.WellExists.DefaultCellStyle = DataGridViewCellStyle4
        Me.WellExists.HeaderText = "Well Exists?"
        Me.WellExists.MinimumWidth = 42
        Me.WellExists.Name = "WellExists"
        Me.WellExists.ReadOnly = True
        Me.WellExists.Width = 42
        '
        'DateExists
        '
        Me.DateExists.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.NullValue = False
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DateExists.DefaultCellStyle = DataGridViewCellStyle5
        Me.DateExists.HeaderText = "Date Exists?"
        Me.DateExists.MinimumWidth = 42
        Me.DateExists.Name = "DateExists"
        Me.DateExists.ReadOnly = True
        Me.DateExists.Width = 42
        '
        'colImport
        '
        Me.colImport.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        Me.colImport.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.colImport.HeaderText = "Import?"
        Me.colImport.MinimumWidth = 45
        Me.colImport.Name = "colImport"
        Me.colImport.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colImport.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colImport.Width = 45
        '
        'IDWell
        '
        Me.IDWell.HeaderText = "wvWellHeader.idwell"
        Me.IDWell.Name = "IDWell"
        Me.IDWell.ReadOnly = True
        Me.IDWell.Visible = False
        '
        'ExcelRow
        '
        Me.ExcelRow.HeaderText = "Excel Row"
        Me.ExcelRow.Name = "ExcelRow"
        Me.ExcelRow.Visible = False
        '
        'WellHeaderDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 462)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lbl_intro)
        Me.Controls.Add(Me.btn_import)
        Me.Controls.Add(Me.btn_cancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MinimizeBox = False
        Me.Name = "WellHeaderDialog"
        Me.Text = "Production Import Selection"
        CType(Me.dgvWellSelect, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvWellSelect As System.Windows.Forms.DataGridView
    Friend WithEvents btn_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_import As System.Windows.Forms.Button
    Friend WithEvents lbl_intro As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tp_selectAll As System.Windows.Forms.ToolTip
    Friend WithEvents tp_deselectAll As System.Windows.Forms.ToolTip
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents WellName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DtTm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WellExists As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DateExists As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colImport As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IDWell As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ExcelRow As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Imports Peloton.AppFrame.IO

Module modGlobal
    Private mIO As IOEngine
    Private msIODBConn As String = ""
    Private msTitle As String = "WLR Production Data Import"
    Private msTag As String = "WLRProdImport"
    Private mExportFileDir As String = ""

    Public Property IOEngine() As IOEngine
        Get
            Return mIO
        End Get
        Set(ByVal value As IOEngine)
            mIO = value
            msIODBConn = ConnFromIO()
        End Set
    End Property

    Public ReadOnly Property IODBConnection As String
        Get
            Return msIODBConn
        End Get
    End Property

    Public Property Title() As String
        Get
            Return msTitle
        End Get
        Set(ByVal sValue As String)
            msTitle = sValue
        End Set
    End Property

    Public Property Tag() As String
        Get
            Return msTag
        End Get
        Set(ByVal value As String)
            msTag = value
        End Set
    End Property

    Public Property ExportFileDir() As String
        Get
            Return mExportFileDir
        End Get
        Set(ByVal value As String)
            mExportFileDir = value
        End Set
    End Property

    Private Function ConnFromIO() As String
        'set up the connection
        Dim sConn As String = ""

        If Not mIO Is Nothing Then
            Select Case mIO.ConnectionDefinition.Provider
                Case DBMS.Access
                    sConn = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Persist Security Info=False;", mIO.ConnectionDefinition.Server)
                Case DBMS.Oracle
                    sConn = String.Format("Provider=OraOLEDB.Oracle;Data Source={2};User Id={0};Password={1};", mIO.ConnectionDefinition.User, mIO.ConnectionDefinition.Password, mIO.ConnectionDefinition.Server)
                Case DBMS.SQLServer, DBMS.SQLExpress
                    sConn = String.Format("Provider=SQLOLEDB;Server={0};Database={1};", mIO.ConnectionDefinition.Server, mIO.ConnectionDefinition.Database)
                    If mIO.ConnectionDefinition.Trusted Then
                        sConn = String.Format("{0}Trusted_Connection=yes;", sConn)
                    Else
                        sConn = String.Format("{0}UID={1};PWD={2};", sConn, mIO.ConnectionDefinition.User, mIO.ConnectionDefinition.Password)
                    End If
            End Select
        End If

        Return sConn
    End Function
End Module


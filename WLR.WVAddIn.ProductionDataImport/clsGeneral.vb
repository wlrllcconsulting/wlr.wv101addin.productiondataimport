Public Class clsGeneral
    Private moConnection As New OleDb.OleDbConnection
    Private msConnection As String = ""
    Private msIniFile As String = ""

    Public Property DBConnection() As String
        Get
            Return msConnection
        End Get
        Set(ByVal value As String)
            msConnection = value
            If Not (moConnection Is Nothing) OrElse moConnection.State <> ConnectionState.Closed Then
                moConnection.Close()
            End If
            moConnection = New OleDb.OleDbConnection(msConnection)
            moConnection.Open()
        End Set
    End Property

    Public Sub DBFillDS(ByRef dsData As DataSet, ByVal sTable As String, ByVal sSQL As String)
        Dim oCommand As OleDb.OleDbCommand
        Dim oDataAdapter As OleDb.OleDbDataAdapter
        oCommand = New OleDb.OleDbCommand(sSQL, moConnection)
        oDataAdapter = New OleDb.OleDbDataAdapter(oCommand)
        oDataAdapter.Fill(dsData, sTable)
    End Sub

    Public Function Data_ReturnStringUptoMaxLength(ByVal value As String, ByVal maxLength As Integer) As String
        If value.Length <= maxLength Then
            Return value
        Else
            Return value.Substring(0, maxLength)
        End If
    End Function

    Public Function FileDirectory() As String
        'Gets the current file directory
        Dim file As String = System.Reflection.Assembly.GetExecutingAssembly.GetName().CodeBase
        Dim uriBuild As UriBuilder = New UriBuilder(file)
        file = System.IO.Path.GetDirectoryName(Uri.UnescapeDataString(uriBuild.Path))
        Return file
    End Function

    Private Function FileIni() As String
        Dim file As String = System.Reflection.Assembly.GetExecutingAssembly.Location.ToString.Replace(".dll", ".ini")
        Return file
    End Function

    Public Function IniResultCollection(ByVal sSection As String) As Dictionary(Of String, String)
        If msIniFile.Length = 0 Then msIniFile = FileIni()
        Return parseIni(msIniFile, sSection)
    End Function

    Public Function IniResultByKey(ByVal sSection As String, ByVal sKey As String) As String
        Dim lstResults As Dictionary(Of String, String) = IniResultCollection(sSection)
        Dim sResults As String = ""
        If lstResults.ContainsKey(sKey) Then sResults = lstResults.Item(sKey)
        Return sResults.Trim
    End Function

    Public Function IniResult(ByVal sSection As String) As String
        Dim lstResults As Dictionary(Of String, String) = IniResultCollection(sSection)
        Dim sResults As String = ""
        For Each oKey As KeyValuePair(Of String, String) In lstResults
            sResults += oKey.Value + " "
        Next
        Return sResults.Trim
    End Function

    ''' <summary>
    ''' Parses the supplied .INI file by section or keyword and return matching key/value pairs as Dictionary(of string, string)
    ''' </summary>
    ''' <param name="strFilePath">Full file path of the .INI file</param>
    ''' <param name="strSection">Specific section of .INI file to search for. Do not include the brackets.</param>
    ''' <param name="isSection">Optionally specify whether the search is for Section or Keyword</param>
    ''' <returns>"Nothing" is returned if the .INI file can not be found</returns>
    ''' <remarks>This assumes that the .INI file is defined in key/value format such as key=123.</remarks>
    Private Function parseIni(ByVal strFilePath As String, ByVal strSection As String, Optional ByVal isSection As Boolean = True) As Dictionary(Of String, String)

        Dim strSearch As String = ""
        Dim dicSectionKeyValue As New Dictionary(Of String, String)

        'Add brackets to the keyword if searching by section
        If isSection = True Then
            strSearch = String.Format("[{0}]", strSection.ToLower.Trim)
        Else
            strSearch = strSection.ToLower.Trim
        End If

        Try
            If (Not String.IsNullOrEmpty(strFilePath)) AndAlso (Not String.IsNullOrEmpty(strSearch)) Then
                If System.IO.File.Exists(strFilePath) Then
                    Dim oReader As New System.IO.StreamReader(strFilePath)
                    Select Case isSection
                        Case True
                            'Read until end of document
                            Do While oReader.Peek() <> -1
                                Dim strCurrentLine As String = oReader.ReadLine.ToLower
                                'Find matched section
                                If strCurrentLine.IndexOf(strSearch) >= 0 Then
                                    'Advance to the first key under the matched section
                                    strCurrentLine = oReader.ReadLine
                                    'Find all children key/value pairs until the matched section ends
                                    Do Until String.IsNullOrEmpty(strCurrentLine) OrElse strCurrentLine.StartsWith(";") OrElse strCurrentLine.StartsWith("[")
                                        Dim strRaw As String() = strCurrentLine.Split("=")
                                        'Add key/value pairs only if they have value
                                        If strRaw.Length > 1 AndAlso Not String.IsNullOrEmpty(strRaw(0)) AndAlso Not String.IsNullOrEmpty(strRaw(1)) Then
                                            dicSectionKeyValue.Add(strRaw(0), strRaw(1))
                                        End If
                                        strCurrentLine = oReader.ReadLine
                                    Loop
                                End If
                            Loop
                        Case False
                            Dim strCurrentLine As String = ""
                            'Read until end of document
                            Do While oReader.Peek() <> -1
                                strCurrentLine = oReader.ReadLine.ToLower
                                'Find matching key/value pairs 
                                If strCurrentLine.IndexOf(strSearch) >= 0 AndAlso Not strCurrentLine.StartsWith(";") AndAlso Not strCurrentLine.StartsWith("[") Then
                                    Dim strRaw As String() = strCurrentLine.Split("=")
                                    'Add key/value pairs only if they have value
                                    If strRaw.Length > 1 AndAlso Not String.IsNullOrEmpty(strRaw(0)) AndAlso Not String.IsNullOrEmpty(strRaw(1)) Then
                                        dicSectionKeyValue.Add(strRaw(0), strRaw(1))
                                    End If
                                End If
                            Loop
                        Case Else
                    End Select

                    oReader.Close()
                    oReader.Dispose()

                    Return dicSectionKeyValue

                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw New Exception("Ini File Error: " + ex.Message)
            Return Nothing
        End Try
    End Function
End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReviewLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnl_log = New System.Windows.Forms.Panel()
        Me.btn_viewLog = New System.Windows.Forms.Button()
        Me.btn_saveLog = New System.Windows.Forms.Button()
        Me.btn_close = New System.Windows.Forms.Button()
        Me.diag_saveLog = New System.Windows.Forms.SaveFileDialog()
        Me.txtLog = New System.Windows.Forms.TextBox()
        Me.txtLogView = New System.Windows.Forms.TextBox()
        Me.pnl_log.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnl_log
        '
        Me.pnl_log.AutoScroll = True
        Me.pnl_log.Controls.Add(Me.txtLogView)
        Me.pnl_log.Location = New System.Drawing.Point(5, 177)
        Me.pnl_log.Name = "pnl_log"
        Me.pnl_log.Size = New System.Drawing.Size(680, 410)
        Me.pnl_log.TabIndex = 2
        Me.pnl_log.Visible = False
        '
        'btn_viewLog
        '
        Me.btn_viewLog.Location = New System.Drawing.Point(96, 148)
        Me.btn_viewLog.Name = "btn_viewLog"
        Me.btn_viewLog.Size = New System.Drawing.Size(75, 23)
        Me.btn_viewLog.TabIndex = 3
        Me.btn_viewLog.Text = "View Log"
        Me.btn_viewLog.UseVisualStyleBackColor = True
        '
        'btn_saveLog
        '
        Me.btn_saveLog.Location = New System.Drawing.Point(177, 148)
        Me.btn_saveLog.Name = "btn_saveLog"
        Me.btn_saveLog.Size = New System.Drawing.Size(75, 23)
        Me.btn_saveLog.TabIndex = 4
        Me.btn_saveLog.Text = "Save Log"
        Me.btn_saveLog.UseVisualStyleBackColor = True
        '
        'btn_close
        '
        Me.btn_close.Location = New System.Drawing.Point(258, 148)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(75, 23)
        Me.btn_close.TabIndex = 5
        Me.btn_close.Text = "Close"
        Me.btn_close.UseVisualStyleBackColor = True
        '
        'diag_saveLog
        '
        Me.diag_saveLog.DefaultExt = "html"
        Me.diag_saveLog.Filter = "Text files (*.txt)|*.txt|Web page (*.html)|*.html|All files (*.*)|*.*"
        Me.diag_saveLog.RestoreDirectory = True
        Me.diag_saveLog.Title = "Save File As"
        '
        'txtLog
        '
        Me.txtLog.Location = New System.Drawing.Point(5, 12)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.ReadOnly = True
        Me.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLog.Size = New System.Drawing.Size(327, 130)
        Me.txtLog.TabIndex = 6
        '
        'txtLogView
        '
        Me.txtLogView.Location = New System.Drawing.Point(3, 3)
        Me.txtLogView.Multiline = True
        Me.txtLogView.Name = "txtLogView"
        Me.txtLogView.ReadOnly = True
        Me.txtLogView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLogView.Size = New System.Drawing.Size(673, 403)
        Me.txtLogView.TabIndex = 0
        '
        'ReviewLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(339, 177)
        Me.Controls.Add(Me.pnl_log)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_saveLog)
        Me.Controls.Add(Me.btn_viewLog)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ReviewLog"
        Me.Text = "Import Complete"
        Me.pnl_log.ResumeLayout(False)
        Me.pnl_log.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pnl_log As System.Windows.Forms.Panel
    Friend WithEvents btn_viewLog As System.Windows.Forms.Button
    Friend WithEvents btn_saveLog As System.Windows.Forms.Button
    Friend WithEvents btn_close As System.Windows.Forms.Button
    Friend WithEvents diag_saveLog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents txtLog As System.Windows.Forms.TextBox
    Friend WithEvents txtLogView As System.Windows.Forms.TextBox
End Class

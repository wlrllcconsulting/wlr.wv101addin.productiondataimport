﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_fileLocation = New System.Windows.Forms.TextBox()
        Me.btn_browse = New System.Windows.Forms.Button()
        Me.diag_browseImportFile = New System.Windows.Forms.OpenFileDialog()
        Me.btn_import = New System.Windows.Forms.Button()
        Me.groupBox_SelectFile = New System.Windows.Forms.GroupBox()
        Me.lbl_fileLocation = New System.Windows.Forms.Label()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.groupBox_SelectFile.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_fileLocation
        '
        Me.txt_fileLocation.Location = New System.Drawing.Point(6, 51)
        Me.txt_fileLocation.Name = "txt_fileLocation"
        Me.txt_fileLocation.Size = New System.Drawing.Size(429, 20)
        Me.txt_fileLocation.TabIndex = 1
        '
        'btn_browse
        '
        Me.btn_browse.Location = New System.Drawing.Point(360, 77)
        Me.btn_browse.Name = "btn_browse"
        Me.btn_browse.Size = New System.Drawing.Size(75, 23)
        Me.btn_browse.TabIndex = 2
        Me.btn_browse.Text = "Browse..."
        Me.btn_browse.UseVisualStyleBackColor = True
        '
        'diag_browseImportFile
        '
        '
        'btn_import
        '
        Me.btn_import.Location = New System.Drawing.Point(263, 142)
        Me.btn_import.Name = "btn_import"
        Me.btn_import.Size = New System.Drawing.Size(92, 24)
        Me.btn_import.TabIndex = 6
        Me.btn_import.Text = "Import"
        Me.btn_import.UseVisualStyleBackColor = True
        '
        'groupBox_SelectFile
        '
        Me.groupBox_SelectFile.Controls.Add(Me.lbl_fileLocation)
        Me.groupBox_SelectFile.Controls.Add(Me.txt_fileLocation)
        Me.groupBox_SelectFile.Controls.Add(Me.btn_browse)
        Me.groupBox_SelectFile.Location = New System.Drawing.Point(12, 12)
        Me.groupBox_SelectFile.Name = "groupBox_SelectFile"
        Me.groupBox_SelectFile.Size = New System.Drawing.Size(441, 112)
        Me.groupBox_SelectFile.TabIndex = 7
        Me.groupBox_SelectFile.TabStop = False
        Me.groupBox_SelectFile.Text = "Select File to Import"
        '
        'lbl_fileLocation
        '
        Me.lbl_fileLocation.AutoSize = True
        Me.lbl_fileLocation.Location = New System.Drawing.Point(7, 32)
        Me.lbl_fileLocation.Name = "lbl_fileLocation"
        Me.lbl_fileLocation.Size = New System.Drawing.Size(92, 13)
        Me.lbl_fileLocation.TabIndex = 3
        Me.lbl_fileLocation.Text = "Import file location"
        '
        'btn_cancel
        '
        Me.btn_cancel.Location = New System.Drawing.Point(361, 142)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(92, 24)
        Me.btn_cancel.TabIndex = 8
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'ImportDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(465, 179)
        Me.Controls.Add(Me.btn_cancel)
        Me.Controls.Add(Me.btn_import)
        Me.Controls.Add(Me.groupBox_SelectFile)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportDialog"
        Me.Text = "WLR - Well Production Import"
        Me.groupBox_SelectFile.ResumeLayout(False)
        Me.groupBox_SelectFile.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_fileLocation As System.Windows.Forms.TextBox
    Friend WithEvents btn_browse As System.Windows.Forms.Button
    Friend WithEvents diag_browseImportFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btn_import As System.Windows.Forms.Button
    Friend WithEvents groupBox_SelectFile As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_fileLocation As System.Windows.Forms.Label
    Friend WithEvents btn_cancel As System.Windows.Forms.Button
End Class

﻿
Public Class ImportDialog
    Private mclsGeneral As New clsGeneral

    Private Sub btn_browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_browse.Click
        callFileBrowser()
    End Sub

    Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        Me.Close()
    End Sub

    Private Sub btn_import_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_import.Click
        If txt_fileLocation.Text.Length > 0 Then
            If System.IO.File.Exists(txt_fileLocation.Text) Then
                Dim frmWell As New WellHeaderDialog(txt_fileLocation.Text)
                frmWell.ShowDialog()
                Me.Close()
            Else
                System.Windows.Forms.MessageBox.Show("Import file does not exist.  Please check file path and name.", "File", System.Windows.Forms.MessageBoxButtons.OK)
            End If
        Else
            System.Windows.Forms.MessageBox.Show("Please select a file to import.", "File", System.Windows.Forms.MessageBoxButtons.OK)
        End If

    End Sub

    Private Sub diag_browseImportFile_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles diag_browseImportFile.FileOk

        Me.txt_fileLocation.Text = diag_browseImportFile.FileName.ToString


    End Sub

    Private Sub callFileBrowser()

        diag_browseImportFile.Title = "Please Select a File to Import"
        diag_browseImportFile.Filter = "Excel Files (*.xl*;*.xlsx;*.xlsm;*.xlsb;*.xlam;*.xltx;*.xltm;*.xls;*.xla;*.xlt;*.xlm;*.xlw)|*.xl*;*.xlsx;*.xlsm;*.xlsb;*.xlam;*.xltx;*.xltm;*.xls;*.xla;*.xlt;*.xlm;*.xlw|Text Files (*.txt; *.csv)|*.txt;*.csv"
        diag_browseImportFile.ShowDialog()

    End Sub

End Class